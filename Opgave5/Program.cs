﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Opgave5
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Dash optional, cant have birthday on 00 or month 00 forkert !
            string cprPattern = "^((?!00)[0-9]{2}){2}[0-9]{2}-?[0-9]{4}$";
            string input;
            
            Console.WriteLine("INPUT CPR");

            do
            {
                input = Console.ReadLine();

                Match m = Regex.Match(input, cprPattern);
                if (m.Success)
                    Console.WriteLine("FOUND CPR!");
                else
                    Console.WriteLine("NOT CPR! TRY AGAIN!");

            } while (input != "q");  //Hej
        }
    }
}